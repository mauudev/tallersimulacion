/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.*;
import javax.swing.JOptionPane;

/**
 *
 * @author MauDev
 */
public class GeneradorMultiplicativoDecimal {
    public GeneradorMultiplicativoDecimal(){}
    private static boolean esPrimo(long n){
        boolean res;
        if(n<1) res = false;
        else if(n == 1 || n == 2 || n == 3) res = true;
        else if(n % 2 == 0) res = false;
        else res = esPrimo(n, 3);
        return res;
    }
    private static boolean esPrimo(long n , long k){
        boolean res;
        if(k<= n/2){
            if(n % k == 0) res = false;
            else res = esPrimo(n, k+2);
        }else res = true;
        return res;
    }
    
    /*private static int mcm(int a, int b){
        int n1, n2, mod=0, mcm=0;
        n1 = a;
        n2 = b;
        while(a != b){
            if(a > b) a = a - b;
            else b = b - a;
        }
        mod = b;
        mcm = n1*n2/mod;
        return mcm;
    }
    private static int mcd(int a, int b) {
       if(b==0)
           return a;
       else
           return mcd(b, a % b);
    }*/ 
    
    
    public static long mcd(long a, long b){
        while (b > 0){
            long temp = b;
            b = a % b; 
            a = temp;
        }
        return a;
    }
    public static long mcd(Integer[] input){
        long result = (int)input[0];
        for(int i = 1; i < input.length; i++) result = mcd(result, (long)input[i]);
        return result;
    }
    public static long mcm(long a, long b){
        long res = a * (b / mcd(a, b));
        return res;
    }
    public static long mcm(Integer[] input){
        long result = (long)input[0];
        for(int i = 1; i < input.length; i++) result = mcm(result, (long)input[i]);
        return result;
    }


    private static long retornarPotenciaDe10(long numero){
        return retornarPotenciaDe10(numero,0);
    }
    private static long retornarPotenciaDe10(long numero, long n){
        long res = n;
        if(n >= 25)return -1;
        else{
        if(numero >= 10 && n <= 24){
            if(Math.pow(10,n) == numero){
                return res;
            }else res = retornarPotenciaDe10(numero,n+1);
        }else return res;
        }
        return res;
    }   
    private static boolean sonPrimos(long a, long b){
        return mcd(a,b) == 1;
    }
    private static long[] descomponerFact(long n){
        long [] res = new long[10];
        return descomponerFact(n,2,res,0);
    }
    private static long[] descomponerFact(long n, long k, long[] res, int i){
        if(n != 1 && i < res.length-1)
            if(n % k == 0){
                res[i] = k;
                res = descomponerFact(n/k, k,res, i+1);
            }else res = descomponerFact(n, k+1,res,i);
        return res;
    }
    private static HashMap<Integer,Integer> mapearValores(long n){
        HashMap<Integer,Integer> valores = new HashMap<Integer,Integer>();
        long[] arr = descomponerFact(n); 
        return mapearValores(n,arr,0,0,arr[0],valores);
    }
    private static HashMap<Integer,Integer> mapearValores(long n, long[] arr, int count, int i,long actual,
                                                            HashMap<Integer,Integer> valores){
        if(i < arr.length-1){
            if(arr[i] == actual)
                valores = mapearValores(n,arr,count+1,i+1,actual,valores);
            else{
                valores.put((int)arr[i-1],count);
                actual = arr[i];
                valores = mapearValores(n,arr,1,i+1,actual,valores);
            }
        }
        return valores;
    }
    private static ArrayList<Integer> getValoresPeriodo(long n){
       ArrayList<Integer> res = new ArrayList<Integer>();
       HashMap<Integer,Integer> hash = mapearValores(n);
       Iterator it = hash.entrySet().iterator();
       int i = 0;
       while (it.hasNext()) {
           Map.Entry e = (Map.Entry)it.next();       
           res.add(((int)(e.getKey())-1)*(int)(Math.pow((int)(e.getKey()),(int)(e.getValue())-1)));
           i++;
       }
       return res;
    }
    public static long getPeriodoGM(long a, long x0, long m){
        long periodo = -1;
        long d = 0;
        if(x0%2 == 0 && x0%5 == 0 && !sonPrimos(x0,m))
            return periodo;
        else{
            d = retornarPotenciaDe10(m);
            if(d != -1){
                if(d > 5){
                    periodo = 5 * (int)((Math.pow(10,d-2)));
                    return periodo;
                }else{
                    ArrayList<Integer> valores = getValoresPeriodo(m);
                    Integer [] tmp = valores.toArray(new Integer[valores.size()]);
                    if(tmp.length > 0)
                        periodo = mcm(tmp);
                    }
                }
            }
        return periodo;
        }
    public static void main(String[] args){
        int opcion;
   
        Scanner entrada=new Scanner(System.in);
        System.out.println("Seleccione una opcion: ");
        System.out.println("1.- Determinar la longitud de periodo para un ejercicio ");
        System.out.println("2.- Generar los numeros aleatorios para un ejercicio ");
        System.out.println("3.- Salir ");
        opcion = entrada.nextInt();
        long a,x0,m;
        switch(opcion){
            case 1:
                a = Integer.parseInt(JOptionPane.showInputDialog("Ingrese valor de a:"));
                x0 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese valor de x0 semilla:"));
                m = Integer.parseInt(JOptionPane.showInputDialog("Ingrese valor de m:"));
                System.out.println("a = "+a+" x0 = "+x0+" m = "+m);
                System.out.println("La longitud de periodo para este ejercicio es: "+getPeriodoGM(a,x0,m));
            break;
            case 2:
                a = Integer.parseInt(JOptionPane.showInputDialog("Ingrese valor de a:"));
                x0 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese valor de x0 semilla:"));
                m = Integer.parseInt(JOptionPane.showInputDialog("Ingrese valor de m:"));
                long periodo = getPeriodoGM(a, x0, m);
                imprimirValoresGM(periodo,a,x0,m);
            break;
            case 3:
                System.out.println("Adios !");
            break;
            default: System.out.println("Opcion invalida !");
            break;
        }
    }  
    public static void imprimirValoresGM(long periodo, long a, long x0, long m){
        if(periodo == -1) System.out.println("Error el valor de a no cumple las propiedades de Generador Congruencial Multiplicativo");
        else{ 
            System.out.println("a = "+a+" x0 = "+x0+" m = "+m);
            System.out.println("********************");
            System.out.println("n   Xn    aXn   Xn+1");
            System.out.println("********************");
            int i = 0;
            long xn = x0;
            long axn = 0;
            long xn1 = 0;
            double parteDecimal = 0;
            while(periodo > 0){
                axn = xn * a;
                parteDecimal = (((double)axn/(double)m)%1)*m;
                long xn_old = xn;
                xn = (long)Math.round(parteDecimal);
                System.out.println(i+"   "+xn_old+"    "+axn+"    "+(long)Math.round(parteDecimal));
                i++;
                periodo--;
            }
        }
    }
}    